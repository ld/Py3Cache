# -*- coding: utf-8 -*-

"""
Python Implements Level 2 Cache Broker
"""
import json
import os
import sys
import threading
import random

import redis

from py3cache import PyInMem

if sys.version < '3':
    import ConfigParser as configparser
else:
    import configparser


class Py3Cache (threading.Thread):

    OPT_JOIN = 1   # 加入集群
    OPT_EVICT = 2  # 删除缓存 key
    OPT_CLEAR = 3  # 清除缓存 region
    OPT_QUIT = 4   # 退出集群

    client_id = ''
    memory_cache = ''
    redis_conn = ''     # Refer to Instance of redis connection
    redis_pubsub = ''   # Redis Pub/Sub Channel
    redis_channel = ''
    debug = False

    def __init__(self):
        self.client_id = random.randint(1000000, 9999999)
        threading.Thread.__init__(self)
        config = configparser.ConfigParser()
        config.optionxform = str  # 保留配置文件key的大小写设定
        config_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "config.ini")
        config.read(config_path)

        self.debug = "true" == config.get("global", "debug")

        opts = config.items('memory')
        self.memory_cache = PyInMem.PyInMem(opts, callback=self.callback)

        redis_host = config.get("redis", "host")
        redis_port = config.get("redis", "port")
        redis_db = config.getint("redis", "db")
        self.redis_channel = config.get("redis", "channel")

        try:
            self.redis_conn = redis.Redis(host=redis_host, port=redis_port, db=redis_db)

            self.publish(self.OPT_JOIN)

            self.redis_pubsub = self.redis_conn.pubsub()
            self.redis_pubsub.subscribe([self.redis_channel])
            self.start()

            if self.debug:
                rci = self.redis_conn.info()
                print("Connected to Redis channel {0}".format(self.redis_channel), {"version": rci['redis_version'], "platform": rci['os']})

        except Exception as e:
            print("Initialize Redis Failed:", e)
            sys.exit()

    # 内存数据失效时的回调函数
    def callback(self, action, region, key=None):
        if action == 'evict':
            self.publish(self.OPT_EVICT, region, key)
        elif action == 'clear':
            self.publish(self.OPT_CLEAR, region)
        else:
            print("Unknown callback action={0},region={1},key={2}".format(action, region, key))

    # 线程轮训 Redis Pub/Sub 收到的消息
    def run(self):
        for item in self.redis_pubsub.listen():
            # print(item['data'].__class__)
            if isinstance(item['data'], bytes):
                # channel = item['channel'].decode()
                msg = json.loads(item['data'].decode())
                print(msg)
                src = msg['src']
                if src != self.client_id:
                    operator = msg['operator']
                    if operator == self.OPT_JOIN:
                        print("Node-{0} joined to cluster: {1}".format(src, self.redis_channel));
                    elif operator == self.OPT_CLEAR:
                        self.memory_cache._clear(msg['region'])
                    elif operator == self.OPT_EVICT:
                        self.memory_cache._evict(msg['region'], msg['keys'][0])
                    elif operator == self.OPT_QUIT:
                        print("Node-{0} quited from cluster: {1}".format(src, self.redis_channel));
                    elif self.debug:
                        print("Unknown message:", msg)

    def close(self):
        self.publish(self.OPT_QUIT)
        self.redis_pubsub.unsubscribe()

    def publish(self, action, region=None, key=None):
        self.redis_conn.publish(self.redis_channel, self.__action(action, region, key))

    """ Get Data From Py3Cache """
    def get(self, region, key):
        val = self.memory_cache.get(region, key)
        if val:
            return json.loads(val)

        redis_val = self.redis_conn.hget(region, key)
        if redis_val:
            val = json.loads(redis_val)

        if val:
            self.memory_cache.set(region, key, redis_val)
            return val

        return None

    def put(self, region, key, val):
        cache = json.dumps(val)
        self.memory_cache.set(region, key, cache)
        self.redis_conn.hset(region, key, cache)
        ''' Broadcast evict message to all Py3Cache nodes'''
        self.publish(self.OPT_EVICT, region, key)

    def evict(self, region, key):
        self.redis_conn.hdel(region, key)
        self.memory_cache.evict(region, key)
        ''' Broadcast evict message to all Py3Cache nodes'''
        self.publish(self.OPT_EVICT, region, key)

    def clear(self, region):
        self.redis_conn.delete(region)
        self.memory_cache.clear(region)
        ''' Broadcast clear message to all Py3Cache nodes'''
        self.publish(self.OPT_CLEAR, region)

    def __action(self, action, region=None, key=None):
        msg = {"operator": action, "region": region, "keys": [key], "src": self.client_id}
        return json.dumps(msg)


if __name__ == '__main__':
    p3c = Py3Cache()

    while True:
        s_input = input("> ")
        if s_input == 'quit' or s_input == 'exit':
            break
        cmds = s_input.split()
        if cmds[0] == 'get':
            data = p3c.get(cmds[1], cmds[2])
            print('[{0},{1}] => {2}'.format(cmds[1], cmds[2], data))
        elif cmds[0] == 'set':
            p3c.put(cmds[1], cmds[2], cmds[3])
            print('[{0},{1}] <= {2}'.format(cmds[1], cmds[2], cmds[3]))
        elif cmds[0] == 'evict':
            p3c.evict(cmds[1], cmds[2])
            print('[{0},{1}] <= None'.format(cmds[1], cmds[2]))
        elif cmds[0] == 'clear':
            p3c.clear(cmds[1])
            print('[{0},*] <= None'.format(cmds[1]))
        else:
            print('Unknown command:', s_input)

    p3c.close()
    sys.exit(0)
